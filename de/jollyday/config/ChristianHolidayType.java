//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ChristianHolidayType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ChristianHolidayType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="GOOD_FRIDAY"/&gt;
 *     &lt;enumeration value="EASTER_MONDAY"/&gt;
 *     &lt;enumeration value="ASCENSION_DAY"/&gt;
 *     &lt;enumeration value="WHIT_MONDAY"/&gt;
 *     &lt;enumeration value="CORPUS_CHRISTI"/&gt;
 *     &lt;enumeration value="MAUNDY_THURSDAY"/&gt;
 *     &lt;enumeration value="ASH_WEDNESDAY"/&gt;
 *     &lt;enumeration value="MARDI_GRAS"/&gt;
 *     &lt;enumeration value="GENERAL_PRAYER_DAY"/&gt;
 *     &lt;enumeration value="CLEAN_MONDAY"/&gt;
 *     &lt;enumeration value="SHROVE_MONDAY"/&gt;
 *     &lt;enumeration value="PENTECOST"/&gt;
 *     &lt;enumeration value="CARNIVAL"/&gt;
 *     &lt;enumeration value="EASTER_SATURDAY"/&gt;
 *     &lt;enumeration value="EASTER_TUESDAY"/&gt;
 *     &lt;enumeration value="SACRED_HEART"/&gt;
 *     &lt;enumeration value="EASTER"/&gt;
 *     &lt;enumeration value="PENTECOST_MONDAY"/&gt;
 *     &lt;enumeration value="WHIT_SUNDAY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ChristianHolidayType")
@XmlEnum
public enum ChristianHolidayType {

    GOOD_FRIDAY,
    EASTER_MONDAY,
    ASCENSION_DAY,
    WHIT_MONDAY,
    CORPUS_CHRISTI,
    MAUNDY_THURSDAY,
    ASH_WEDNESDAY,
    MARDI_GRAS,
    GENERAL_PRAYER_DAY,
    CLEAN_MONDAY,
    SHROVE_MONDAY,
    PENTECOST,
    CARNIVAL,
    EASTER_SATURDAY,
    EASTER_TUESDAY,
    SACRED_HEART,
    EASTER,
    PENTECOST_MONDAY,
    WHIT_SUNDAY;

    public String value() {
        return name();
    }

    public static ChristianHolidayType fromValue(String v) {
        return valueOf(v);
    }

}
