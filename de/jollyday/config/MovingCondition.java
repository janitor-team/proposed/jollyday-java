//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MovingCondition complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MovingCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="substitute" type="{http://www.example.org/Holiday}Weekday" /&gt;
 *       &lt;attribute name="with" type="{http://www.example.org/Holiday}With" /&gt;
 *       &lt;attribute name="weekday" type="{http://www.example.org/Holiday}Weekday" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MovingCondition")
public class MovingCondition {

    @XmlAttribute(name = "substitute")
    protected Weekday substitute;
    @XmlAttribute(name = "with")
    protected With with;
    @XmlAttribute(name = "weekday")
    protected Weekday weekday;

    /**
     * Ruft den Wert der substitute-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Weekday }
     *     
     */
    public Weekday getSubstitute() {
        return substitute;
    }

    /**
     * Legt den Wert der substitute-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Weekday }
     *     
     */
    public void setSubstitute(Weekday value) {
        this.substitute = value;
    }

    /**
     * Ruft den Wert der with-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link With }
     *     
     */
    public With getWith() {
        return with;
    }

    /**
     * Legt den Wert der with-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link With }
     *     
     */
    public void setWith(With value) {
        this.with = value;
    }

    /**
     * Ruft den Wert der weekday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Weekday }
     *     
     */
    public Weekday getWeekday() {
        return weekday;
    }

    /**
     * Legt den Wert der weekday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Weekday }
     *     
     */
    public void setWeekday(Weekday value) {
        this.weekday = value;
    }

}
