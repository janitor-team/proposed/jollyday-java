//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EthiopianOrthodoxHoliday complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EthiopianOrthodoxHoliday"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.example.org/Holiday}Holiday"&gt;
 *       &lt;attribute name="type" type="{http://www.example.org/Holiday}EthiopianOrthodoxHolidayType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EthiopianOrthodoxHoliday")
public class EthiopianOrthodoxHoliday
    extends Holiday
{

    @XmlAttribute(name = "type")
    protected EthiopianOrthodoxHolidayType type;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EthiopianOrthodoxHolidayType }
     *     
     */
    public EthiopianOrthodoxHolidayType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EthiopianOrthodoxHolidayType }
     *     
     */
    public void setType(EthiopianOrthodoxHolidayType value) {
        this.type = value;
    }

}
