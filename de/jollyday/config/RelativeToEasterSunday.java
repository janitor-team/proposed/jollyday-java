//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RelativeToEasterSunday complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RelativeToEasterSunday"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.example.org/Holiday}Holiday"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="chronology" type="{http://www.example.org/Holiday}ChronologyType"/&gt;
 *         &lt;element name="days" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelativeToEasterSunday", propOrder = {
    "chronology",
    "days"
})
public class RelativeToEasterSunday
    extends Holiday
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChronologyType chronology;
    protected int days;

    /**
     * Ruft den Wert der chronology-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChronologyType }
     *     
     */
    public ChronologyType getChronology() {
        return chronology;
    }

    /**
     * Legt den Wert der chronology-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChronologyType }
     *     
     */
    public void setChronology(ChronologyType value) {
        this.chronology = value;
    }

    /**
     * Ruft den Wert der days-Eigenschaft ab.
     * 
     */
    public int getDays() {
        return days;
    }

    /**
     * Legt den Wert der days-Eigenschaft fest.
     * 
     */
    public void setDays(int value) {
        this.days = value;
    }

}
