//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.jollyday.config package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Configuration_QNAME = new QName("http://www.example.org/Holiday", "Configuration");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.jollyday.config
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Configuration }
     * 
     */
    public Configuration createConfiguration() {
        return new Configuration();
    }

    /**
     * Create an instance of {@link RelativeToFixed }
     * 
     */
    public RelativeToFixed createRelativeToFixed() {
        return new RelativeToFixed();
    }

    /**
     * Create an instance of {@link Holidays }
     * 
     */
    public Holidays createHolidays() {
        return new Holidays();
    }

    /**
     * Create an instance of {@link Fixed }
     * 
     */
    public Fixed createFixed() {
        return new Fixed();
    }

    /**
     * Create an instance of {@link FixedWeekdayInMonth }
     * 
     */
    public FixedWeekdayInMonth createFixedWeekdayInMonth() {
        return new FixedWeekdayInMonth();
    }

    /**
     * Create an instance of {@link RelativeToWeekdayInMonth }
     * 
     */
    public RelativeToWeekdayInMonth createRelativeToWeekdayInMonth() {
        return new RelativeToWeekdayInMonth();
    }

    /**
     * Create an instance of {@link IslamicHoliday }
     * 
     */
    public IslamicHoliday createIslamicHoliday() {
        return new IslamicHoliday();
    }

    /**
     * Create an instance of {@link ChristianHoliday }
     * 
     */
    public ChristianHoliday createChristianHoliday() {
        return new ChristianHoliday();
    }

    /**
     * Create an instance of {@link FixedWeekdayBetweenFixed }
     * 
     */
    public FixedWeekdayBetweenFixed createFixedWeekdayBetweenFixed() {
        return new FixedWeekdayBetweenFixed();
    }

    /**
     * Create an instance of {@link MovingCondition }
     * 
     */
    public MovingCondition createMovingCondition() {
        return new MovingCondition();
    }

    /**
     * Create an instance of {@link HinduHoliday }
     * 
     */
    public HinduHoliday createHinduHoliday() {
        return new HinduHoliday();
    }

    /**
     * Create an instance of {@link HebrewHoliday }
     * 
     */
    public HebrewHoliday createHebrewHoliday() {
        return new HebrewHoliday();
    }

    /**
     * Create an instance of {@link EthiopianOrthodoxHoliday }
     * 
     */
    public EthiopianOrthodoxHoliday createEthiopianOrthodoxHoliday() {
        return new EthiopianOrthodoxHoliday();
    }

    /**
     * Create an instance of {@link FixedWeekdayRelativeToFixed }
     * 
     */
    public FixedWeekdayRelativeToFixed createFixedWeekdayRelativeToFixed() {
        return new FixedWeekdayRelativeToFixed();
    }

    /**
     * Create an instance of {@link RelativeToEasterSunday }
     * 
     */
    public RelativeToEasterSunday createRelativeToEasterSunday() {
        return new RelativeToEasterSunday();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Configuration }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Configuration }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.example.org/Holiday", name = "Configuration")
    public JAXBElement<Configuration> createConfiguration(Configuration value) {
        return new JAXBElement<Configuration>(_Configuration_QNAME, Configuration.class, null, value);
    }

}
