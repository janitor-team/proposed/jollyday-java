//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IslamicHolidayType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="IslamicHolidayType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NEWYEAR"/&gt;
 *     &lt;enumeration value="ASCHURA"/&gt;
 *     &lt;enumeration value="MAWLID_AN_NABI"/&gt;
 *     &lt;enumeration value="LAILAT_AL_MIRAJ"/&gt;
 *     &lt;enumeration value="LAILAT_AL_BARAT"/&gt;
 *     &lt;enumeration value="RAMADAN"/&gt;
 *     &lt;enumeration value="LAILAT_AL_QADR"/&gt;
 *     &lt;enumeration value="RAMADAN_END"/&gt;
 *     &lt;enumeration value="ID_AL_FITR"/&gt;
 *     &lt;enumeration value="ID_AL_FITR_2"/&gt;
 *     &lt;enumeration value="ID_AL_FITR_3"/&gt;
 *     &lt;enumeration value="ARAFAAT"/&gt;
 *     &lt;enumeration value="ID_UL_ADHA"/&gt;
 *     &lt;enumeration value="ID_UL_ADHA_2"/&gt;
 *     &lt;enumeration value="ID_UL_ADHA_3"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IslamicHolidayType")
@XmlEnum
public enum IslamicHolidayType {

    NEWYEAR,
    ASCHURA,
    MAWLID_AN_NABI,
    LAILAT_AL_MIRAJ,
    LAILAT_AL_BARAT,
    RAMADAN,
    LAILAT_AL_QADR,
    RAMADAN_END,
    ID_AL_FITR,
    ID_AL_FITR_2,
    ID_AL_FITR_3,
    ARAFAAT,
    ID_UL_ADHA,
    ID_UL_ADHA_2,
    ID_UL_ADHA_3;

    public String value() {
        return name();
    }

    public static IslamicHolidayType fromValue(String v) {
        return valueOf(v);
    }

}
